import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        String listOfTVbroadcast[] = {"Новости", "Фильм.Боевик", "Фильм.Мелодрама", "Фильм.Ужастик",
                "Фильм.Триллер", "Фильм.Военный", "Выступление Президента",
                "Cлухи и сплетни", "Дом-3", "Сериал"};

        String listOfChannels[] = {"ОРТ", "НТВ", "Россия-1", "ТНТ", "СТС", "РЕН-TV", "ТВ-3",
                "СПАС", "МУЗ-ТВ", "Пятый"};

        TVBroadcast[] arrayOfTVBroadcast = new TVBroadcast[listOfTVbroadcast.length];
        Channel[] arrayOfChannels = new Channel[listOfChannels.length];

        for (int i = 0; i < listOfTVbroadcast.length; i++) {
            arrayOfTVBroadcast[i] = new TVBroadcast(listOfTVbroadcast[i]);
        }

        for (int i = 0; i < listOfChannels.length; i++) {
            arrayOfChannels[i] = new Channel(listOfChannels[i], arrayOfTVBroadcast);
        }

        TVSet myTV = new TVSet(arrayOfChannels);
        Pult myPult = new Pult(myTV);

        Scanner scanner = new Scanner(System.in);
        int numberForPult = scanner.nextInt();
        while (numberForPult != 100) {
            if (numberForPult == 0) {
                myPult.switchFront();
            } else if (numberForPult == -1) {
                myPult.switchBack();
            } else if (numberForPult == 77) {
                myPult.switchToLast();
            } else myPult.switchByNumber(numberForPult);
            numberForPult = scanner.nextInt();

        }


    }


}


