public class Channel {
    private String channelName;
    private TVBroadcast[] listOfBroadcast;

    public Channel(String channelName, TVBroadcast[] listOfBroadcast) {
        this.channelName = channelName;
        this.listOfBroadcast = listOfBroadcast;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public TVBroadcast[] getListOfBroadcast() {
        return listOfBroadcast;
    }

    public void setListOfBroadcast(TVBroadcast[] listOfBroadcast) {
        this.listOfBroadcast = listOfBroadcast;
    }
}
