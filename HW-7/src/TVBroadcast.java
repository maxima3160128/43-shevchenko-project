public class TVBroadcast {
    private String broadcastName;

    public TVBroadcast(String broadcastName) {
        this.broadcastName = broadcastName;
    }

    public String getBroadcastName() {
        return broadcastName;
    }

    public void setBroadcastName(String broadcastName) {
        this.broadcastName = broadcastName;
    }
}
