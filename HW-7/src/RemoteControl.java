public interface RemoteControl {

    public void switchByNumber(int number);

    public void switchFront();

    public void switchBack();

    public void switchToLast();

}
