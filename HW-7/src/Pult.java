public class Pult implements RemoteControl {

    TVSet tvSet;

    public Pult(TVSet tvSet) {
        this.tvSet = tvSet;
    }

    @Override
    public void switchByNumber(int number) {
        tvSet.switchByNumber(number);
    }

    @Override
    public void switchFront() {
        tvSet.switchFront();
    }

    @Override
    public void switchBack() {
        tvSet.switchBack();
    }

    @Override
    public void switchToLast() {
        tvSet.switchToLast();
    }

}
