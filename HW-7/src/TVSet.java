public class TVSet {

    private Channel[] channels;
    int currentChannel;
    int lastChannel;


    public TVSet(Channel[] channels) {
        this.channels = channels;
        this.currentChannel = ((int) Math.random() * getChannels().length);
        printChannel();
    }

    public Channel[] getChannels() {
        return channels;
    }

    public void setChannels(Channel[] channels) {
        this.channels = channels;
    }

    public void switchByNumber(int number) {
        lastChannel = currentChannel;
        if (number >= getChannels().length) {
            currentChannel = 0;
        } else currentChannel = number;
        printChannel();
    }

    public void switchFront() {
        lastChannel = currentChannel;
        if (currentChannel == getChannels().length - 1) {
            currentChannel = 0;
        } else currentChannel++;
        printChannel();
    }

    public void switchBack() {
        lastChannel = currentChannel;
        if (currentChannel == 0) {
            currentChannel = getChannels().length - 1;
        } else currentChannel--;
        printChannel();
    }

    public void switchToLast() {
        int temp = currentChannel;
        currentChannel = lastChannel;
        lastChannel = temp;
        printChannel();
    }

    private void printChannel() {
        System.out.println("Канал номер " + currentChannel + "  " + getChannels()[currentChannel].getChannelName() +
                "\n  " + getChannels()[currentChannel].
                getListOfBroadcast()[(int) (Math.random() * getChannels().length)].
                getBroadcastName() + "\n прошлый канал " + lastChannel);
    }


}
