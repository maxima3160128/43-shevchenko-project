import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CarDAOImpl implements CarDAO {
    private List<Car> carList = new ArrayList<>();
    private File file;

    public CarDAOImpl(File file) {
        this.file = file;
        this.carList = this.getAll();
    }

    @Override
    public List<Car> getAll() {
        List<Car> carListForReturn = new ArrayList<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null) {
                carListForReturn.add(lineToCar(line));
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return carListForReturn;
    }

    @Override
    public void addCar(Car car) {
        carList = getAll();
        car.setId(getLastId() + 1);
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(file, true));
            //bw.write(carToLine(car));
            bw.write(car.toString()); //ex carToLIne
            bw.flush();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void delete(int id) {
        carList = getAll();
        boolean flagExistId = false;
        for (Car c : carList) {
            if (c.getId() == id) {
                carList.remove(c);
                flagExistId = true;
                writeCarsToFile();
                return;
            }
        }
        if (!flagExistId) {
            System.out.println("Нет такой машины в списке");
            return;
        }
    }

    @Override
    public void updateCar(Car car) {
        carList = getAll();
        carList.get(car.getId()).setBrand(car.getBrand());
        carList.get(car.getId()).setModel(car.getModel());
        carList.get(car.getId()).setColor(car.getColor());
        carList.get(car.getId()).setEnginePower(car.getEnginePower());

        writeCarsToFile();
    }

    @Override
    public String getByID(int id) {
        for (Car c : carList) {
            if (c.getId() == id) return c.toString();  //ex carToLine
        }
        return null;
    }

    public List<String> getAllCarsInLines() {
        carList = getAll();
        List<String> lines = new ArrayList<>();
        for (int i = 0; i < carList.size(); i++) {
            lines.add((carList.get(i)).toString()); //ex carToLine
        }
        return lines;
    }

    private void writeCarsToFile() {
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(file));
            String line;
            for (Car с : carList) {
                bw.write(с.toString());
            }
            bw.flush();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private Car lineToCar(String line) {
        String[] parts = line.split(" ");
        int id = Integer.parseInt(parts[0].trim());
        String brand = parts[1];
        String model = parts[2];
        String color = parts[3];
        int enginePower = Integer.parseInt(parts[4].trim());
        Car car = new Car(id, brand, model, color, enginePower);
        return car;
    }

    private int getLastId() {
        List<Car> listOfCars = getAll();

        return listOfCars.get(listOfCars.size() - 1).getId();
    }
}

