import java.io.File;
import java.util.List;

public interface CarDAO {

    List<Car> getAll();

    void addCar(Car car);


    void delete(int id);

    void updateCar(Car car);

    String getByID (int id);
}
