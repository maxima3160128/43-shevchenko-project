import java.util.UUID;

public class Car {
    private static int COUINT_ID = 0;
    private int id = 1;
    private String brand;
    private String model;
    private String color;
    private int enginePower;

    public Car(int id, String brand, String model, String color, int enginePower) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.color = color;
        this.enginePower = enginePower;
        COUINT_ID++;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getEnginePower() {
        return enginePower;
    }

    public void setEnginePower(int enginePower) {
        this.enginePower = enginePower;
    }

    @Override
    public String toString() {
        return id + " " + brand + " " + model + " " +
                color + " " +
                enginePower;
    }
}
