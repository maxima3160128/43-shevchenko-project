import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        String filePath = "resources/cars.txt";
        File file = new File(filePath);
        Scanner scanner = new Scanner(System.in);
        List<Car> carList = new ArrayList<>();
        CarDAOImpl cardaoimpl = new CarDAOImpl(file);

        try {
            // if (!file.exists()) {
            System.out.println(file.createNewFile());
            //}
        } catch (IOException exception) {
            System.out.println(exception.getMessage());
        }

        System.out.println("Для вывода списка машин введите 1 ");
        System.out.println("Для поиска  машины по ID введите 2 ");
        System.out.println("Для добавления машины введите 3 ");
        System.out.println("Для редактирования  машины введите 4 ");
        System.out.println("Для удаления машины введите 5 ");
        System.out.println("Для выхода нажмите -1");

        int numberForChoice = scanner.nextInt();

        while (numberForChoice != -1) {
            if (numberForChoice == 1) {

                List<String> listOfCars = new ArrayList<>();
                listOfCars = cardaoimpl.getAllCarsInLines();
                listOfCars.forEach(System.out::println);
                System.out.println("Выбирете пункт главного меню:");

            } else if (numberForChoice == 2) {
                System.out.println(findCarById(cardaoimpl));
                System.out.println("Выбирете пункт главного меню:");
            } else if (numberForChoice == 3) {
                addCar(cardaoimpl);
                System.out.println("Выбирете пункт главного меню:");
            } else if (numberForChoice == 4) {
                editCar(cardaoimpl);
                System.out.println("Выбирете пункт главного меню:");
            } else if (numberForChoice == 5) {
                deleteCar(cardaoimpl);
                System.out.println("Выбирете пункт главного меню:");
            }
            numberForChoice = scanner.nextInt();
        }
    }

    public static void editCar (CarDAOImpl cDAO) {
        Scanner cIn = new Scanner(System.in);

        System.out.println("Введите id автомобиля, который будете редактировать: ");
        int id = cIn.nextInt();

        cIn.nextLine();

        System.out.println("Введите марку автомобиля:");
        String brand = cIn.nextLine();

        System.out.println("Введите модель автомобиля:");
        String model = cIn.nextLine();

        System.out.println("Введите цвет автомобиля:");
        String color = cIn.nextLine();

        System.out.println("Введите мощность двигателя цифрами: ");
        int powerEngine = cIn.nextInt();
        Car car = new Car(id, brand, model, color, powerEngine);
        cDAO.updateCar(car);
    }
    public static void addCar(CarDAOImpl cDAO) {
        Scanner conIn = new Scanner(System.in);

        System.out.println("Контроль введеных вами данных не производится !!!");
        System.out.println("Введите марку автомобиля:");
        String brand = conIn.nextLine();

        System.out.println("Введите модель автомобиля:");
        String model = conIn.nextLine();

        System.out.println("Введите цвет автомобиля:");
        String color = conIn.nextLine();

        System.out.println("Введите мощность двигателя цифрами: ");
        int powerEngine = conIn.nextInt();

        Car car = new Car(0, brand, model, color, powerEngine);
        cDAO.addCar(car);
    }

    public static String findCarById(CarDAOImpl cDAO) {
        System.out.println("Введите id автомобиля");
        Scanner consoleIn = new Scanner(System.in);
        int id = consoleIn.nextInt();
        return cDAO.getByID(id);
    }

    public static void deleteCar(CarDAOImpl cDAO) {
        System.out.println("Введите id автомобиля");
        Scanner conIn = new Scanner(System.in);
        int id = conIn.nextInt();
        cDAO.delete(id);
    }

}