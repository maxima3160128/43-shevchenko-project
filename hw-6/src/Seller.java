public class Seller extends Worker {
     private int workHoursPerWeek = 60;

    public Seller(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }


    @Override
    public void goToWork() {
        super.goToWork();
        System.out.println(" работает " + workHoursPerWeek + " часов в неделю и продаёт всё, что скажут.");
    }

    @Override
    public void goToVacation(int days) {
        super.goToVacation(days);
        System.out.println(" на " + days + " дней с ящиком пива!");
    }
}
