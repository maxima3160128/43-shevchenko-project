public class Main {
    public static void main(String[] args) {

        Worker boss = new Boss("Аль", "Капоне", "Босс");
        Worker manager = new Manager("Джон", "Смит", "менеджер");
        Worker accountant = new Accountant("Лесли", "Шамуэль ", "бухгалтер");
        Worker seller = new Seller("Билл", "Мюррей", "продавец");


        boss.goToWork();
        boss.goToVacation(10);

        manager.goToWork();
        manager.goToVacation(12);

        accountant.goToWork();
        accountant.goToVacation(14);

        seller.goToWork();
        seller.goToVacation(7);


    }
}