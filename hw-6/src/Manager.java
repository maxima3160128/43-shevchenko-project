public class Manager extends Worker {
     private int workHoursPerWeek = 40;

    public Manager(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }


    @Override
    public void goToWork() {
        super.goToWork();
        System.out.println(" работает " + workHoursPerWeek + " часов в неделю и ищет самый дешевый товар.");
    }

    @Override
    public void goToVacation(int days) {
        super.goToVacation(days);
        System.out.println(" на " + days + " дней с семьёй ");
    }
}
