public class Boss extends Worker {
     private int workHoursPerWeek = 20;


    public Boss(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }


    @Override
    public void goToWork() {
        super.goToWork();
        System.out.println(" работает " + workHoursPerWeek + " часов в неделю.");
    }

    @Override
    public void goToVacation(int days) {
        super.goToVacation(days);
        System.out.println(" на " + days + " дней с секретаршей. ");
    }
}
