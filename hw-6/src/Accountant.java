public class Accountant extends Worker {

    private int workHoursPerWeek = 50;

    public Accountant(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }


    @Override
    public void goToWork() {
        super.goToWork();
        System.out.println(" работает " + workHoursPerWeek + " часов в неделю и делает годовой отчёт.");
    }

    @Override
    public void goToVacation(int days) {
        super.goToVacation(days);
        System.out.println(" на " + days + " дней с годовым отчётом.");
    }
}
