import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class Main {
    public static void main(String[] args) throws InterruptedException {

        Semaphore semaphore = new Semaphore(4);
        FaceControl auntGalya = new FaceControl("тетя Галя", semaphore);
        Factory factory = new Factory(auntGalya);
        int quantityWorkers = 10;

        for (int i = 0; i < quantityWorkers; i++) {
            new Worker("Worker" + i, factory);
        }
    }
}