import java.util.concurrent.Semaphore;
import java.util.function.Consumer;

public class FaceControl {
    private String securityName;
    private Semaphore semaphore;

    public FaceControl(String securityName, Semaphore semaphore) {
        this.securityName = securityName;
        this.semaphore = semaphore;
    }

    public void inputWorker () throws InterruptedException {
        semaphore.acquire();
    }

    public void outWorker () {
        semaphore.release();
    }
    public String getSecurityName() {
        return securityName;
    }

    public void setSecurityName(String securityName) {
        this.securityName = securityName;
    }

}
