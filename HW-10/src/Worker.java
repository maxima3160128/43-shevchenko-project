import java.util.concurrent.Semaphore;

public class Worker extends Thread {
    private final String workerName;
    private Factory factory;

    public Worker(String workerName, Factory factory) {
        this.workerName = workerName;
        this.factory = factory;
        System.out.println(this.workerName + " пришел на завод.");
        System.out.println(this.workerName + " ждёт пропуск.");
        start();
    }

    public String getWorkerName() {
        return workerName;
    }

    @Override
    public void run() {
        try {
            factory.inToFactory();
            Thread.sleep(500);
            System.out.println(getWorkerName() + " получил пропуск.");
            Thread.sleep(500);
            System.out.println(getWorkerName() + " работает.");
            Thread.sleep(2000);
            System.out.println(getWorkerName() + " отдал пропуск.");
            factory.outFromFactory();
            Thread.sleep(500);
            System.out.println(getWorkerName() + " ушёл домой.");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

    }
}
