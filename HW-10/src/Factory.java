import java.util.concurrent.Semaphore;

public class Factory {
    private FaceControl security;

    public Factory(FaceControl security) {
        this.security = security;
    }


    public void inToFactory () throws InterruptedException {
      security.inputWorker();
    }

    public void outFromFactory () {
        security.outWorker();
    }

    public FaceControl getSecurity() {
        return security;
    }

    public void setSecurity(FaceControl security) {
        this.security = security;
    }
}
