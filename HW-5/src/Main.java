public class Main {
    public static void main(String[] args) {

        String[] names = {"Иван", "Пётр", "Павел", "Андрей", "Иосиф", "Александр", "Герасим", "Мойша"};
        String[] surnames = {"Иванов", "Петров", "Сидоров", "Кузнецов", "Джугашвили", "Кузнецов", "Рабинович"};

        int sizeOfArray = (int) (Math.random() * 20);

        Human[] humans = new Human[sizeOfArray];

        for (int i = 0; i < humans.length; i++) {
            String name = names[(int) (Math.random() * names.length)];
            String surname = surnames[(int) (Math.random() * surnames.length)];
            int age = (int) (Math.random() * 100);
            humans[i] = new Human(name, surname, age);
        }

        for (int i = 0; i < humans.length; i++) {
            System.out.println(" Человек №" + i + " : " + humans[i].getName() + " " + humans[i].getSurname() +
                    " и возраста " + humans[i].getAge() + " лет");
        }

        Human tmp = new Human();
        for (int i = 0; i < humans.length; i++) {
            for (int j = i + 1; j < humans.length; j++) {
                if (humans[i].getAge() > humans[j].getAge()) {
                    tmp = humans[i];
                    humans[i] = humans[j];
                    humans[j] = tmp;

                }
            }
        }
        System.out.println("\n Сортируем людей по возрастанию возраста: ");
        for (int i = 0; i < humans.length; i++) {
            System.out.println(" Человек №" + i + " : " + humans[i].getName() + " " + humans[i].getSurname() +
                    " и возраста " + humans[i].getAge() + " лет");
        }

    }
}