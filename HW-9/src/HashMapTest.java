import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class HashMapTest {
    public static void main(String[] args) {

        Map<String, Integer> wordsForAnalize = new HashMap();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите строку для анализа:");

        String[] words = scanner.nextLine().split(" ");

        for (int i = 0; i < words.length; i++) {
            if (wordsForAnalize.containsKey(words[i])) {
                int count = wordsForAnalize.get(words[i]);
                wordsForAnalize.put(words[i], ++count);
            } else {
                wordsForAnalize.put(words[i], 1);
            }
        }

        wordsForAnalize.forEach((key, value) ->
                        System.out.println("Слово: " + key + " - " + value + " раз.") );
    }
}
