package Number;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Main {
    public static void main(String[] args) {

        Set<Numbers> numberSet = new HashSet<Numbers>();
        Scanner scanner = new Scanner(System.in);
        String regionAuto;
        String numberAuto;


        while (true) {
            System.out.println(" Введите регион: ");
            regionAuto = scanner.nextLine();
            System.out.println(" Введите номер авто: ");
            numberAuto = scanner.nextLine();

            Numbers newNumber = new Numbers(numberAuto, regionAuto);
            if (numberSet.add(newNumber)) {
                System.out.println("Ваш номер зарегистрирован");
            } else System.out.println("Такой номер существует, введите другой");
        }
    }
}
