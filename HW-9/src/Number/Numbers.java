package Number;

import java.util.Objects;

public class Numbers {

    private String value;
    private String region;

    public Numbers(String value, String region) {
        this.value = value;
        this.region = region;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Numbers numbers = (Numbers) o;
        return Objects.equals(value, numbers.value) && Objects.equals(region, numbers.region);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, region);
    }
}
