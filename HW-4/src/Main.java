import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите порядковое число числа Фиббоначи :");
        int number = scanner.nextInt();
        while (number < 0) {
            System.out.println("Число должно быть БОЛЬШЕ 0");
            number = scanner.nextInt();
        }
        System.out.println("Число Фиббоначи по рекурсии: " + fibRecursive(number));
        System.out.println("Число Фиббоначи простой функцией5: " + fibSimple(number));


    }

    public static int fibRecursive(int number) {
        if (number == 1 || number == 2) {
            return 1;
        } else {
            return fibRecursive(number - 1) + fibRecursive(number - 2);
        }

    }

    public static int fibSimple(int number) {
        int result = 0;
        if (number == 1 || number == 2) return 1;
        else {
            int num1 = 1;
            int num2 = 1;
            for (int i = 3; i <= number; i++) {
                result = num1 + num2;
                num1 = num2;
                num2 = result;
            }
            return result;
        }

    }
}