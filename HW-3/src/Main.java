public class Main {
    public static void main(String[] args) {

        int[] arrayForSorting = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};

        System.out.println(" Первоночальный вид массива:");
        for (int i = 0; i < arrayForSorting.length; i++) {
            System.out.print(arrayForSorting[i] + " ");
        }

        int indexOfArray=0;
        for (int i = 0; i < arrayForSorting.length; i++) {
            if(arrayForSorting[i] != 0) {
                arrayForSorting[indexOfArray] = arrayForSorting[i];
                indexOfArray++;
            }
        }

        for (int i = indexOfArray; i < arrayForSorting.length; i++) {
            arrayForSorting[i] =0;
        }


        System.out.println("\n Конечный вид массива: ");
        for (int i = 0; i < arrayForSorting.length; i++) {
            System.out.print(arrayForSorting[i] + " ");
        }


    }
}